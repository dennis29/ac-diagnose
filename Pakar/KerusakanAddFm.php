<?php 
include "inc.session.php"; 
include "menu.php"; 
include "../librari/inc.koneksidb.php";
include "../librari/inc.kodeauto.php";

# Membaca data pada form, lalu datanya ditampilkan sebagai Value form
$TxtKode  		= kdauto("kerusakan","K");
$TxtGejala 		= isset($_POST['TxtNama']) ? $_POST['TxtNama'] : ''; 
$Txtkerusakan 	= isset($_POST['Txtkerusakan']) ? $_POST['Txtkerusakan'] : ''; 
$TxtKeterangan 	= isset($_POST['TxtKeterangan']) ? $_POST['TxtKeterangan'] : ''; 
$TxtSolusi 		= isset($_POST['TxtSolusi']) ? $_POST['TxtSolusi'] : ''; 
?>
<html>
<head>
<title>Tambah Data Kerusakan</title>
</head>
<body>
<form name="form1" method="post" action="kerusakanAddSim.php">
<table width="600" border="0" cellpadding="2" cellspacing="1" bgcolor="#DBEAF5">
<tr> 
  <td colspan="2" bgcolor="#77B6D0"><b>TAMBAH DATA KERUSAKAN</b></td>
</tr>
<tr bgcolor="#FFFFFF"> 
  <td>Kode</td>
  <td><input name="TxtKode" type="text"  maxlength="4" size="6" value="<?php echo $TxtKode; ?>" disabled="disabled">
  <input name="TxtKodeH" type="hidden" value="<?php echo $TxtKode; ?>"></td>
</tr>
<tr bgcolor="#FFFFFF"> 
  <td width="135">Nama Kerusakan</td>
  <td width="454"><input name="Txtkerusakan" type="text" value="<?php echo $Txtkerusakan; ?>" size="60" maxlength="100"></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td>Keterangan</td>
  <td><textarea name="TxtKeterangan" cols="50" rows="4"><?php echo $TxtKeterangan; ?></textarea></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td>Solusi</td>
  <td><textarea name="TxtSolusi" cols="50" rows="4"><?php echo $TxtSolusi; ?></textarea></td>
</tr>
<tr bgcolor="#FFFFFF"> 
  <td>&nbsp;</td>
  <td><input type="submit" name="Submit" value="Simpan"></td>
</tr>
</table>
</form>
</body>
</html>
