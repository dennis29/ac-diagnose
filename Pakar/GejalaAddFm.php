<?php 
include "inc.session.php"; 
include "menu.php"; 
include "../librari/inc.koneksidb.php";
include "../librari/inc.kodeauto.php";

# Membaca data pada form, lalu datanya ditampilkan sebagai Value form
$TxtKode  = kdauto("gejala","G");
$TxtGejala = isset($_POST['TxtNama']) ? $_POST['TxtNama'] : ''; 
?>
<html>
<head>
<title>Tambah Data Gejala</title>
</head>
<body>
<form name="form1" method="post" action="GejalaAddSim.php">
  <table width="600" border="0" cellpadding="2" cellspacing="1" bgcolor="#DBEAF5">
    <tr> 
      <td colspan="2" bgcolor="#77B6D0"><b>TAMBAH DATA GEJALA</b></td>
    </tr>
    <tr bgcolor="#FFFFFF"> 
      <td>Kode</td>
      <td><input name="TxtKode" type="text"  maxlength="4" size="10" value="<?php echo $TxtKode; ?>" disabled="disabled">
	      <input name="TxtKodeH" type="hidden" value="<?php echo $TxtKode; ?>"></td>
    </tr>
    <tr bgcolor="#FFFFFF"> 
      <td width="110">Nama Gejala</td>
      <td width="479"><textarea name="TxtGejala" cols="50" rows="3"><?php echo $TxtGejala; ?></textarea></td>
    </tr>
    <tr bgcolor="#FFFFFF"> 
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="Simpan"></td>
    </tr>
  </table>
</form>
</body>
</html>
