<?php
// isset dipakai untuk memastikan keberadaan variabel page di URL
// isset hanya ada di PHP 5 pada Xampp 1.8 ke atas
if(isset($_GET['page'])) {
	// Membaca page dari URL
	$page= $_GET['page'];
	if ($page=="dafrusak") {
		if(file_exists ("kerusakanTampil.php")) {
			include "kerusakanTampil.php";
		}
		else {
			echo "FILE PROGRAM KERUSAKAN TIDAK ADA";
		}
	}
	elseif ($page=="dafgejala") {
		if(file_exists ("GejalaTampil.php")) {
			include "GejalaTampil.php";
		}
		else {
			echo "FILE PROGRAM GEJALA KERUSAKAN TIDAK ADA";
		}
	}
	elseif ($page=="daftar") {
		if(file_exists ("PasienAddFm.php")) {
			include "PasienAddFm.php";
		}
		else {
			echo "FILE PROGRAM FORM PASIEN TIDAK ADA";
		}
	}
	elseif ($page=="daftarsim") {
		if(file_exists ("PasienAddSim.php")) {
			include "PasienAddSim.php";
		}
		else {
			echo "FILE PROGRAM FORM PASIEN SIMPAN TIDAK ADA";
		}
	}
	elseif ($page=="konsul") {
		if(file_exists ("KonsultasiFm.php")) {
			include "KonsultasiFm.php";
		}
		else {
			echo "FILE PROGRAM FORM KONSULTASI TIDAK ADA";
		}
	}
	elseif ($page=="konsulcek") {
		if(file_exists ("KonsultasiPeriksa.php")) {
			include "KonsultasiPeriksa.php";
		}
		else {
			echo "FILE PROGRAM KONSULTASI PERIKSA TIDAK ADA";
		}
	}
	elseif ($page=="hasil") {
		if(file_exists ("AnalisaHasil.php")) {
			include "AnalisaHasil.php";
		}
		else {
			echo "FILE PROGRAM ANALISA HASIL TIDAK ADA";
		}
	}
	elseif ($page=="tentangSaya") {
		if(file_exists ("aboutme.html")) {
			include "aboutme.html";
		}
		else {
			echo "FILE PROGRAM TENTANG SAYA BELUM ADA";
		}
	}
	elseif ($page=="tentangPerusahaan") {
		if(file_exists ("tentangPerusahaan.htm")) {
			include "tentangPerusahaan.htm";
		}
		else {
			echo "FILE TENTANG PERUSAHAAN TIDAK ADA";
		}
	}
	elseif ($page=="") {
		if(file_exists ("utamakita.htm")) {
			include "utamakita.htm";
		}
		else {
			echo "FILE HALAMAN UTAMA KITA TIDAK ADA";
		}
	}
}
else 
{
	if(file_exists ("kerusakanTampil.php"))
		{
			include "kerusakanTampil.php";
		}
	else
		{
			echo "FILE PROGRAM KERUSAKAN TIDAK ADA";
		}
}
?>