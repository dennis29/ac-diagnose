<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- Pav Icon -->
	<link rel="icon" href="images/icon/home2.jpg" type="image/x-icon" />
	<link rel="shortcut icon" href="images/icon/home2.jpg" type="image/x-icon" />
<!-- CSS Banner -->
	<link rel="stylesheet" href="styles/homepage.css" type="text/css" />

<!-- JS Banner -->
	<script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.cycle.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.cycle.setup.js"></script>

</head>
<body>
<!-- JS Status Bar -->
	<script type="text/javascript" src="scripts/status_bar2.js"></script>
<!-- Tampilan Banner -->
	<div id="homepage" class="clear"  align="center">
<!-- JS Teks Pelangi -->
	<div id="hpage_slider">
	  <div class="item"><img src="images/banner/1.jpg" alt="" align="middle" width="800" /></div>
	  <div class="item"><img src="images/banner/2.jpg" alt="" align="middle" width="800"/></div>
	  <div class="item"><img src="images/banner/3.jpg" alt="" /></div>
	  </div>
	<table width="899" border="0">
	  <tr>
	    <td width="218" align="center" valign="middle"><a href="?page=dafrusak" ><img src="images/icon/notes.png" 
									alt="Home" width="110" height="60" border="0" align="top" 
									onmouseover="this.src='images/icon/notes_edit.png'"
									onmouseout="this.src='images/icon/notes.png'" /></a></td>
	    <td width="197" align="center" valign="middle"><a href="?page=daftar"><img src="images/icon/light_bulb.png"
												onmouseover="this.src='images/icon/light_bulb_accept.png'"
												onmouseout="this.src='images/icon/light_bulb.png'"
												alt="Product" width="80" height="80" border="0" /></a><a href="?" ></a></td>
	    <td width="127" align="center" valign="middle"><a href="?page=tentangSaya"><img src="images/icon/dollar_gold.png"
												onmouseover="this.src='images/icon/dollar_silver.png'"
												onmouseout="this.src='images/icon/dollar_gold.png'"
												alt="Product" width="60" height="60" border="0" /></a></td>
	    <td width="165" align="center" valign="middle"><a href="?page=tentangPerusahaan"><img src="images/icon/user.png"
											onmouseover="this.src='images/icon/user_accept.png'"
											onmouseout="this.src='images/icon/user.png'"
											alt="About" width="60" height="60" border="0" /></a></td>
	    <td width="170" align="center" valign="middle"><a href="pakar/login.php" target="_blank"><img src="images/icon/lock.png"
											onmouseover="this.src='images/icon/unlock.png'"
											onmouseout="this.src='images/icon/lock.png'"
											alt="About" width="60" height="60" border="0" /></a></td>
      </tr>
	  <tr>
	    <td align="center" valign="middle" class="stylelink">Daftar Kerusakan</td>
	    <td align="center" valign="middle" class="stylelink">Konsultasi</td>
	    <td align="center" valign="middle" class="stylelink">Tentang Saya</td>
	    <td align="center" valign="middle" class="stylelink">Tentang Perusahaan</td>
	    <td align="center" valign="middle" class="stylelink">Masuk Pakar</td>
      </tr>
	  <tr>
	    <td colspan="5" align="right" class="style9"></td>
      </tr>
	  <tr>
	    <td colspan="5" align="center" valign="middle"></td>
	    <td colspan="5" align="center" valign="middle"></td>
      </tr>
	  </table>
	</div>
<!-- End Tampilan Banner -->
</body>
</html>