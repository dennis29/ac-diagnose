-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2014 at 05:27 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pakar_ayamdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `analisa_hasil`
--

CREATE TABLE IF NOT EXISTS `analisa_hasil` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama` varchar(60) NOT NULL,
  `kelamin` enum('P','W') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(60) NOT NULL,
  `kd_kerusakan` char(4) NOT NULL,
  `noip` varchar(60) NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `analisa_hasil`
--

INSERT INTO `analisa_hasil` (`id`, `nama`, `kelamin`, `alamat`, `pekerjaan`, `kd_kerusakan`, `noip`, `tanggal`) VALUES
(1, 'sdf', 'P', 'sdf', 'dsfsd', 'P004', '127.0.0.1', '2010-05-07 16:00:47'),
(2, 'sdfsd', 'P', 'df', 'sdf', 'P005', '127.0.0.1', '2010-05-07 16:11:14'),
(3, 'ssdfs', 'P', 'sdfs', 'sdfsd', 'P003', '127.0.0.1', '2010-05-07 16:18:23'),
(4, 'safdsd', 'P', 'sdfsd', 'sdfsdf', 'P004', '127.0.0.1', '2010-05-07 16:24:22'),
(5, 'Joko', 'P', 'Yogyakarta', 'Pengajar', 'P004', '127.0.0.1', '2014-06-03 22:16:43');

-- --------------------------------------------------------

--
-- Table structure for table `gejala`
--

CREATE TABLE IF NOT EXISTS `gejala` (
  `kd_gejala` varchar(4) NOT NULL DEFAULT '',
  `nm_gejala` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kd_gejala`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gejala`
--

INSERT INTO `gejala` (`kd_gejala`, `nm_gejala`) VALUES
('G001', 'Mata merah'),
('G002', 'Mata berair'),
('G003', 'Mata terasa gatal'),
('G004', 'Mata peka terhadap cahaya'),
('G005', 'Mata mengeluarkan kotoran'),
('G006', 'Mata terasa pedas seperti pasir atau benda asing'),
('G007', 'Kelopak mata bengkak'),
('G008', 'Mata terasa nyeri'),
('G009', 'kelopak mata lengket'),
('G010', 'kerusakan mudah menular pada kedua mata'),
('G011', 'penglihatan menurun'),
('G012', 'penglihatan silau'),
('G013', 'terdapat bercak putih pada selaput bening'),
('G014', 'bulu mata rontok'),
('G015', 'penglihatan kadang terganggu'),
('G016', 'nyeri pada daerah disekitar kantong air mata'),
('G017', 'kantong air mata tampak merah'),
('G018', 'kantong air mata membengkak'),
('G019', 'bila kantong air mata ditekan mengeluarkan nanah'),
('G020', 'demam'),
('G021', 'melihat lingkaran disekeliling cahaya'),
('G022', 'penglihatan mendadak berkabut'),
('G023', 'sakit yang berat pada mata'),
('G024', 'terjadi penglihatan ganda di salah satu mata'),
('G025', 'kesulitan melihat pada malam hari'),
('G026', 'penglihatan seperti berasap atau berkabut atau kabur'),
('G027', 'sering berganti kaca mata'),
('G028', 'terdapat benjolan pada kelopak mata atas atau bawah'),
('G029', 'benjolan pada mata terasa keras'),
('G030', 'benjolan pada mata tidak sakit bila ditekankan'),
('G032', 'Gejala Baru, okay');

-- --------------------------------------------------------

--
-- Table structure for table `pakar`
--

CREATE TABLE IF NOT EXISTS `pakar` (
  `userID` varchar(50) NOT NULL,
  `passID` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pakar`
--

INSERT INTO `pakar` (`userID`, `passID`) VALUES
('pakar', '87b7cf2448de01f22b0c016b272f2ec0'),
('admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `kerusakan`
--

CREATE TABLE IF NOT EXISTS `kerusakan` (
  `kd_kerusakan` char(4) DEFAULT NULL,
  `nm_kerusakan` varchar(100) DEFAULT NULL,
  `keterangan` mediumtext,
  `solusi` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kerusakan`
--

INSERT INTO `kerusakan` (`kd_kerusakan`, `nm_kerusakan`, `keterangan`, `solusi`) VALUES
('P001', 'Konjungtivitis', 'Pullorum Disease disebut juga Bacillary White Diarrhea dan yang lebih popular disebut kerusakan berak kapur atau berak putih.', 'Berikan Master Coliprim dosis: 1 gr/1 ltr air selama 3-4 hari (1/2 hari) berturut-turut. setelah itu berikan Master Vit-Stress selama 3-4 hari untuk membantu proses penyembuhan'),
('P002', 'Ulkus kornea', 'kerusakan Fowl Cholera merupakan kerusakan ayam yang dapat menyerang secara pelan-pelan dan juga dapat menyerang secara mendadak.', 'Berikan Master Kolericid dosis: 1 gr/1 ltr air selama 3-4 hari berturut-turut. berikan Master Vit-Stress dosis: 1 gr/3 ltr air untuk membantu proses penyembuhan'),
('P003', 'Blefaritis', 'kerusakan Avian Influenza, disebut juga kerusakan Fowl Plaque. Pertama kali terjadi di Italia sekitar tahun 1800. Selanjutnya menyebar luas sampai tahun 1930, setelah itu menjadi sporadis dan terlokalisasi terutama di timur tengah.', 'Tidak ada obat.\r\nDianjurkan untuk disingkirkan dan dimusnakan dengan cara dibakar dan bangkainya dikubur.'),
('P004', 'dakriosititis', 'kerusakan Newcastle Disease disebut juga Pseudovogel pest Rhaniket, Pheumoencephalitis, Tortor Furrens, dan di Indonesia popular dengan sebutan tetelo. kerusakan ini pertama kali ditemukan oleh Doyle pada tahun 1927, didaerah Newcastle on Tyne, Inggris.', 'Tidak ada obat. \r\nBerikan vitamin untuk membantu kondisi tubuh.'),
('P005', 'Galukoma', 'kerusakan Fowl Typhoid dikenal sebagai kerusakan tipus ayam, tergolong kerusakan menular.', 'Berikan Neo Terramycin dosis: 2 sendok teh/3,8 ltr air selama 3-4 hari berturut-turut.'),
('P006', 'katarak', 'Coccidosis merupakan kerusakan menular yang ganas, dikalangan para peternak ayam disebut juga kerusakan berak darah. kerusakan ini ditemukan pada tahun 1674.', 'Berikan Master Coliprim dosis: 1gr/1 ltr air selama 3-4 hari (1/2 hari) berturut-turut. setelah pengobatan berikan Vitamin Master Vit-Stress dosis: 1gr/3 ltr selama 3-4 hari berturut-turut.'),
('P007', 'kalazion', 'kerusakan Gumboro, disebut juga Infectious Bursal Disease. Pertama kali ditemukan dan dilaporkan pada tahun 1975 oleh Dr. Csgrove di daerah Gumboro, Deleware, Amerika Serikat.', 'Tidak ada obat.\r\nAir gula 30-50 gr/ltr air dan ditambah Master Vit-Stress dosis: 1 gr/2 ltr air untuk meningkatkan kondisi tubuh.'),
('P008', 'Hordelum', 'fdgdfg', 'fgdfgfdg'),
('P009', 'kerusakan Baru', 'kerusakan Baruuu', 'Solusi bagussss');

-- --------------------------------------------------------

--
-- Table structure for table `relasi`
--

CREATE TABLE IF NOT EXISTS `relasi` (
  `kd_gejala` varchar(50) DEFAULT NULL,
  `kd_kerusakan` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `relasi`
--

INSERT INTO `relasi` (`kd_gejala`, `kd_kerusakan`) VALUES
('G001', 'P001'),
('G002', 'P001'),
('G003', 'P001'),
('G004', 'P001'),
('G005', 'P001'),
('G006', 'P001'),
('G007', 'P001'),
('G008', 'P001'),
('G009', 'P001'),
('G010', 'P001'),
('G001', 'P002'),
('G002', 'P002'),
('G003', 'P002'),
('G004', 'P002'),
('G005', 'P002'),
('G011', 'P002'),
('G012', 'P002'),
('G013', 'P002'),
('G001', 'P003'),
('G002', 'P003'),
('G003', 'P003'),
('G014', 'P003'),
('G015', 'P003'),
('G001', 'P004'),
('G002', 'P004'),
('G016', 'P004'),
('G017', 'P004'),
('G018', 'P004'),
('G019', 'P004'),
('G020', 'P004'),
('G001', 'P005'),
('G021', 'P005'),
('G022', 'P005'),
('G023', 'P005'),
('G004', 'P006'),
('G011', 'P006'),
('G021', 'P006'),
('G024', 'P006'),
('G025', 'P006'),
('G026', 'P006'),
('G027', 'P006'),
('G030', 'P007'),
('G028', 'P007'),
('G029', 'P007'),
('G003', 'P009'),
('G001', 'P009'),
('G005', 'P009'),
('G022', 'P009');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_analisa`
--

CREATE TABLE IF NOT EXISTS `tmp_analisa` (
  `noip` varchar(60) NOT NULL,
  `kd_kerusakan` char(4) NOT NULL,
  `kd_gejala` char(4) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmp_analisa`
--

INSERT INTO `tmp_analisa` (`noip`, `kd_kerusakan`, `kd_gejala`) VALUES
('127.0.0.1', 'P004', 'G020'),
('127.0.0.1', 'P004', 'G019'),
('127.0.0.1', 'P004', 'G018'),
('127.0.0.1', 'P004', 'G017'),
('127.0.0.1', 'P004', 'G016'),
('127.0.0.1', 'P004', 'G002'),
('127.0.0.1', 'P004', 'G001');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_gejala`
--

CREATE TABLE IF NOT EXISTS `tmp_gejala` (
  `noip` varchar(60) NOT NULL,
  `kd_gejala` char(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmp_gejala`
--

INSERT INTO `tmp_gejala` (`noip`, `kd_gejala`) VALUES
('127.0.0.1', 'G001'),
('127.0.0.1', 'G002');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_pasien`
--

CREATE TABLE IF NOT EXISTS `tmp_pasien` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama` varchar(60) NOT NULL,
  `kelamin` enum('P','W') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(60) NOT NULL,
  `noip` varchar(60) NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tmp_pasien`
--

INSERT INTO `tmp_pasien` (`id`, `nama`, `kelamin`, `alamat`, `pekerjaan`, `noip`, `tanggal`) VALUES
(8, 'Joko', 'P', 'Yogyakarta', 'Pengajar', '127.0.0.1', '2014-06-03 22:16:43');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_kerusakan`
--

CREATE TABLE IF NOT EXISTS `tmp_kerusakan` (
  `noip` varchar(60) NOT NULL,
  `kd_kerusakan` char(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmp_kerusakan`
--

INSERT INTO `tmp_kerusakan` (`noip`, `kd_kerusakan`) VALUES
('127.0.0.1', 'P004');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
