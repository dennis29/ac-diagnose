Untuk membuat File SQL, silahkan pakai editor WordPad
Cara 1:
1. Klik kanan file pakar_ayamdb.sql
2. Pilih Open WIth
3. Pilih WordPad


Cara 2:
1. Buka editor WordPad : Start -> Programs -> Accessories -> Wordpad
2. Dari menu File, pilih Open
3. Cari file *.sql yang akan dibuka


Catatan :
Nama databasenya adalah pakar_ayamdb