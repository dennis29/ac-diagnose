<?php 
include "librari/inc.koneksidb.php";
session_start();
$_SESSION['gejala']++;
// ========================================================
// 	KUNCI DARI SISTEM PAKAR ADA DI HALAMAN INI
//  Jika Rumus/Metode Konsultasinya tidak sesuai dengan Keinginan Anda, maka Kami persilahkan Anda untuk mengembangkannya
//  Semoga hal kecil ini bisa menjadi Penyebab awal Kesuksessan Proyek Anda, salam belajar
// ========================================================

# Baca variabel Form (If Register Global ON)
$RbPilih 	= $_POST['RbPilih'];
$TxtKdGejala= $_POST['TxtKdGejala'];

# Mendapatkan No IP
$NOIP 		= $_SERVER['REMOTE_ADDR'];

# Fungsi untuk menambah data ke tmp_analisa
function AddTmpAnalisa($kdgejala, $IP) {
	$sql_sakit = "SELECT relasi.* FROM relasi,tmp_kerusakan 
				  WHERE relasi.kd_kerusakan=tmp_kerusakan.kd_kerusakan 
				  AND noip='$IP' ORDER BY relasi.kd_kerusakan,relasi.kd_gejala";
	$qry_sakit = mysql_query($sql_sakit) or die ("Query error :".mysql_error());
	
	while ($data_sakit = mysql_fetch_array($qry_sakit)) {
		$sqltmp = "INSERT INTO tmp_analisa (noip, kd_kerusakan,kd_gejala)
					VALUES ('$IP','$data_sakit[kd_kerusakan]','$data_sakit[kd_gejala]')";
		mysql_query($sqltmp) or die ("Query error :".mysql_error());
	}
}

# Fungsi hapus tabel tmp_gejala
function AddTmpGejala($kdgejala, $IP) {
	$sql_gejala = "INSERT INTO tmp_gejala (noip,kd_gejala) VALUES ('$IP','$kdgejala')";
	mysql_query($sql_gejala) or die ("Query error :".mysql_error());
}

# Fungsi hapus tabel tmp_sakit
function DelTmpSakit($IP) {
	$sql_del = "DELETE FROM tmp_kerusakan WHERE noip='$IP'";
	mysql_query($sql_del) or die ("Query error :".mysql_error());
}

# Fungsi hapus tabel tmp_analisa
function DelTmpAnlisa($IP) {
	$sql_del = "DELETE FROM tmp_analisa WHERE noip='$IP'";
	mysql_query($sql_del) or die ("Query error :".mysql_error());
}

# PEMERIKSAAN
if ($RbPilih == "YA") {
	$sql_analisa = "SELECT * FROM tmp_analisa 
				WHERE kd_gejala='".$TxtKdGejala."' 
				AND noip='".$NOIP."'";
	$qry_analisa = mysql_query($sql_analisa, $koneksi) or die ("Query error :".mysql_error());
	$data_cek = mysql_num_rows($qry_analisa);
	if ($data_cek > 0) {
	//echo "11111<br/>";
		# Kode saat tmp_analisa tidak kosong
		DelTmpSakit($NOIP);
		$sql_tmp = "SELECT * FROM tmp_analisa 
					WHERE kd_gejala='".$TxtKdGejala."' 
					AND noip='".$NOIP."'";
		$qry_tmp = mysql_query($sql_tmp, $koneksi) or die ("Query error :".mysql_error());
		//echo mysql_num_rows($qry_tmp)."<br/>";
		while ($data_tmp = mysql_fetch_array($qry_tmp)) {
			$sql_rsakit = "SELECT * FROM relasi 
							WHERE kd_kerusakan='".$data_tmp['kd_kerusakan']."' 
							GROUP BY kd_kerusakan";
			$qry_rsakit = mysql_query($sql_rsakit, $koneksi) or die ("Query error :".mysql_error());
			while ($data_rsakit = mysql_fetch_array($qry_rsakit)) {
				// Data kerusakan yang mungkin dimasukkan ke tmp
				$sql_input = "INSERT INTO tmp_kerusakan (noip,kd_kerusakan)
							 VALUES ('".$NOIP."','".$data_rsakit['kd_kerusakan']."')";
				mysql_query($sql_input, $koneksi) or die ("Query error :".mysql_error());
			}
		} 
		// Gunakan Fungsi
		DelTmpAnlisa($NOIP);
		AddTmpAnalisa($TxtKdGejala, $NOIP);
		AddTmpGejala($TxtKdGejala, $NOIP);
	}	
	else {
//	echo "11112";
		# Kode saat tmp_analisa kosong
		$sql_rgejala = "SELECT * FROM relasi WHERE kd_gejala='".$TxtKdGejala."'";
		$qry_rgejala = mysql_query($sql_rgejala, $koneksi) or die ("Query error :".mysql_error());
		while ($data_rgejala = mysql_fetch_array($qry_rgejala)) {
			$sql_rsakit = "SELECT * FROM relasi 
						   WHERE kd_kerusakan='".$data_rgejala['kd_kerusakan']."' 
						   GROUP BY kd_kerusakan";
			$qry_rsakit = mysql_query($sql_rsakit, $koneksi) or die ("Query error :".mysql_error());
			while ($data_rsakit = mysql_fetch_array($qry_rsakit)) {
				// Data kerusakan yang mungkin dimasukkan ke tmp
				$sql_input = "INSERT INTO tmp_kerusakan (noip,kd_kerusakan)
							 VALUES ('".$NOIP."','".$data_rsakit['kd_kerusakan']."')";
				mysql_query($sql_input, $koneksi) or die ("Query error :".mysql_error());
			}
		} 
		// Menggunakan Fungsi
		AddTmpAnalisa($TxtKdGejala, $NOIP);
		AddTmpGejala($TxtKdGejala, $NOIP);
	}
	echo "<meta http-equiv='refresh' content='0; url=index.php?page=konsul'>";
}

if ($RbPilih == "TIDAK") {
		$sql_analisa = "SELECT * FROM tmp_analisa 
				WHERE kd_gejala='".$TxtKdGejala."' 
				AND noip='".$NOIP."'";
	$qry_analisa = mysql_query($sql_analisa, $koneksi) or die ("Query error :".mysql_error());
	$data_cek = mysql_num_rows($qry_analisa);
	if ($data_cek >= 1) {
		# Kode saat tmp_analisa tidak kosong
		$sql_relasi = "SELECT * FROM tmp_analisa WHERE kd_gejala='$TxtKdGejala'";
		$qry_relasi = mysql_query($sql_relasi, $koneksi) or die ("Query error :".mysql_error());
		while($hsl_relasi = mysql_fetch_array($qry_relasi)){
			// Hapus daftar relasi yang sudah tidak mungkin dari tabel tmp
			$sql_deltmp = "DELETE FROM tmp_analisa 
							WHERE kd_kerusakan='$hsl_relasi[kd_kerusakan]' 
							AND noip='$NOIP'";
			mysql_query($sql_deltmp, $koneksi) or die ("Query error :".mysql_error());
			
			// Hapus daftar kerusakan yang sudah tidak ada kemungkinan
			$sql_deltmp2 = "DELETE FROM tmp_kerusakan 
						    WHERE kd_kerusakan='$hsl_relasi[kd_kerusakan]' 
						    AND noip='$NOIP'";
			mysql_query($sql_deltmp2, $koneksi) or die ("Query error :".mysql_error());
		}		
	}
	else {
		# Pindahkan data relsi ke tmp_analisa
		$sql_relasi= "SELECT * FROM relasi ORDER BY kd_kerusakan,kd_gejala";
		$qry_relasi= mysql_query($sql_relasi, $koneksi) or die ("Query error :".mysql_error());
		while($hsl_relasi=mysql_fetch_array($qry_relasi)){
			$sql_intmp = "INSERT INTO tmp_analisa (noip, kd_kerusakan,kd_gejala)
						  VALUES ('$NOIP','$hsl_relasi[kd_kerusakan]',
						  '$hsl_relasi[kd_gejala]')";
			mysql_query($sql_intmp,$koneksi) or die ("Query error :".mysql_error());
			
			// Masukkan data kerusakan yang mungkin terjangkit
			$sql_intmp2 = "INSERT INTO tmp_kerusakan(noip,kd_kerusakan)
						   VALUES ('$NOIP','$hsl_relasi[kd_kerusakan]')";
			mysql_query($sql_intmp2,$koneksi) or die ("Query error :".mysql_error());			
		}
		
		# Hapus tmp_analisa yang tidak sesuai
		$sql_relasi2 = "SELECT * FROM relasi WHERE kd_gejala='$TxtKdGejala'";
		$qry_relasi2 = mysql_query($sql_relasi2, $koneksi) or die ("Query error :".mysql_error());
		while($hsl_relasi2 = mysql_fetch_array($qry_relasi2)){
			$sql_deltmp = "DELETE FROM tmp_analisa 
						   WHERE kd_kerusakan='$hsl_relasi2[kd_kerusakan]' 
						   AND noip='$NOIP'";
			mysql_query($sql_deltmp, $koneksi) or die ("Query error :".mysql_error());
			
			// Hapus kerusakan yang sudah tidak mungkin
			$sql_deltmp2 = "DELETE FROM tmp_kerusakan 
							WHERE kd_kerusakan='$hsl_relasi2[kd_kerusakan]' 
							AND noip='$NOIP'";
			mysql_query($sql_deltmp2, $koneksi) or die ("Query error :".mysql_error());
		}
	}
	echo "<meta http-equiv='refresh' content='0; url=index.php?page=konsul'>";
}
//echo $TxtKdGejala."<br/>".$RbPilih."<br/>".$NOIP 	;
?>
